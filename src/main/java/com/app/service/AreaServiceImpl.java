package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.app.dao.AreaRepository;
import com.app.dao.model.AreaDO;
import com.app.exception.AreaNotFoundException;
import com.app.to.AreaTO;

@Service
public class AreaServiceImpl implements AreaService {

	@Autowired
	private AreaRepository areaRepository;

	
	@Override
	public List<AreaTO> retrieveAllAreas(Integer page, Integer size,
			Sort.Direction direction, String sortProperty) {
		// TODO Auto-generated method stub
		if (size == null) {
			return retrieveAllAreas();
		} else {

			PageRequest request = new PageRequest(page, size, direction,
					sortProperty);
			Page<AreaDO> areaDOs = areaRepository.findAll(request);
			List<AreaTO > areaTOs = new ArrayList<AreaTO>();
			areaDOs.forEach(at -> areaTOs.add(new AreaTO(at)));
			return areaTOs;
		}

	}

	@Override
	public List<AreaTO> retrieveAllAreas(){
		List<AreaDO> areaDOs = areaRepository.findAll();
		return areaDOs.stream().map(at -> new AreaTO(at))
				.collect(Collectors.toList());
	}
			
	@Override
	public void createArea(AreaTO areaTO) {
		// TODO Auto-generated method stub
		AreaDO areaDO  = new AreaDO(areaTO);
		areaRepository.save(areaDO);
		
	
	}
	
	@Override
	public void updateArea(AreaTO areaTO) throws AreaNotFoundException {
		// TODO Auto-generated method stub
		AreaDO areaDO = areaRepository.findOne(areaTO.getId());
		if (areaDO == null)
			throw new AreaNotFoundException(" Area not found id- " + areaTO.getId());
		
		areaDO.setProvinceArea(areaTO.getProvinceArea());
	
		areaRepository.save(areaDO);
		
	
	}

	@Override
	public AreaTO retrieveAreaById(Long id) throws AreaNotFoundException {
		// TODO Auto-generated method stub
		AreaDO areaDO = areaRepository.findOne(id);
		if (areaDO == null)
			throw new AreaNotFoundException(" Area not found id- " + id);
		return new AreaTO(areaDO);

	}
	
	@Override
	public void deleteAreaById(Long id) throws AreaNotFoundException {
		// TODO Auto-generated method stub
		AreaDO areaDO = areaRepository.findOne(id);
		if (areaDO == null)
			throw new AreaNotFoundException(" Area not found id- "+ id);
		areaRepository.delete(areaDO);

	}

}

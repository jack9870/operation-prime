package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.app.dao.ReceiptCodeRepository;
import com.app.dao.model.ReceiptCodeDO;
import com.app.exception.ReceiptNotFoundException;
import com.app.to.ReceiptCodeTO;
//import com.gl.operationprime.dao.model.AreaDO;
// import com.gl.operationprime.to.AreaTO;

@Service
public class ReceiptServiceImpl implements ReceiptService {

	@Autowired
	private ReceiptCodeRepository receiptCodeRepository;

	@Override
	public  List<ReceiptCodeTO> retrieveAllReceipts(Integer page, Integer size,
			Sort.Direction direction, String sortProperty) {
		// TODO Auto-generated method stub
		if (size == null) {
			return retrieveAllReceipts();
		} else {

			PageRequest request = new PageRequest(page, size, direction,
					sortProperty);
			Page<ReceiptCodeDO> areaDOs = receiptCodeRepository.findAll(request);
			List<ReceiptCodeTO> receiptCodeTOs = new ArrayList<ReceiptCodeTO>();
			 areaDOs.forEach(at -> receiptCodeTOs.add(new ReceiptCodeTO(at)));
			 return receiptCodeTOs;
		}

	}
	@Override
	public  List<ReceiptCodeTO> retrieveAllReceipts(){
		List<ReceiptCodeDO> areaDOs = receiptCodeRepository.findAll();
		return areaDOs.stream().map(at -> new ReceiptCodeTO(at))
				.collect(Collectors.toList());
	}

	@Override
	public void createReceipt(ReceiptCodeTO receiptCodeTO) {
		// TODO Auto-generated method stub
		ReceiptCodeDO areaDO  = new ReceiptCodeDO(receiptCodeTO);
		receiptCodeRepository.save(areaDO);
		
	
	}
	
	@Override
	public void updateReceipt(ReceiptCodeTO receiptCodeTO) throws ReceiptNotFoundException {
		// TODO Auto-generated method stub
		ReceiptCodeDO receiptCodeDO = receiptCodeRepository.findOne(receiptCodeTO.getId());
		if (receiptCodeDO == null)
			throw new ReceiptNotFoundException(" receipt not found id- " + receiptCodeTO.getId());
		
		receiptCodeDO.setReceiptCode(receiptCodeTO.getReceiptCode());
		receiptCodeRepository.save(receiptCodeDO);
		
	
	}

	@Override
	public ReceiptCodeTO retrieveReceiptsById(Long id) throws ReceiptNotFoundException {
		// TODO Auto-generated method stub
		ReceiptCodeDO receiptCodeDO = receiptCodeRepository.findOne(id);
		if (receiptCodeDO == null)
			throw new ReceiptNotFoundException(" receipt not found id- " + id);
		return new ReceiptCodeTO(receiptCodeDO);

	}
	
	@Override
	public void deleteReceiptById(Long id) throws ReceiptNotFoundException {
		// TODO Auto-generated method stub
		ReceiptCodeDO receiptCodeDO = receiptCodeRepository.findOne(id);
		if (receiptCodeDO == null)
			throw new ReceiptNotFoundException(" receipt not found id- "+ id);
		receiptCodeRepository.delete(receiptCodeDO);

	}

}

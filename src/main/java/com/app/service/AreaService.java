package com.app.service;

import java.util.List;

import org.springframework.data.domain.Sort;

import com.app.exception.AreaNotFoundException;
import com.app.to.AreaTO;

public interface AreaService {
	 public List<AreaTO> retrieveAllAreas(Integer page, Integer size, Sort.Direction direction, String sortProperty) ;

	AreaTO retrieveAreaById(Long id) throws AreaNotFoundException;

	void createArea(AreaTO areaTO);

	void updateArea(AreaTO areaTO) throws AreaNotFoundException;

	void deleteAreaById(Long id) throws AreaNotFoundException;

	List<AreaTO> retrieveAllAreas();
}

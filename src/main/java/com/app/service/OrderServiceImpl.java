package com.app.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.common.CommonUtil;
import com.app.dao.AreaRepository;
import com.app.dao.OperationAreaRepository;
import com.app.dao.OperationRepository;
import com.app.dao.OrderRepository;
import com.app.dao.model.AreaDO;
import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;
import com.app.dao.model.OrderDO;
import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.to.CreateOrderTO;
import com.app.to.OperationTO;
//import com.gl.operationprime.dao.model.AreaDO;
// import com.gl.operationprime.to.AreaTO;
import com.app.to.OrderResponseTO;
import com.app.to.OrderTO;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OperationRepository operationRepository;
	
	@Autowired 
	private OperationAreaRepository operationAreaRepository;
	

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private AreaRepository areaRepository;

	
	@Override
	public List<OrderTO> findAllOrders(){
		return orderRepository.findAll().stream().map( or ->new OrderTO(or)).collect(Collectors.toList());
		
		
	}
	@Override
	public OrderResponseTO createOrder(CreateOrderTO orderTO) throws 
			OperationNotFoundException, AreaNotFoundException {
		
		AreaDO areaDO = areaRepository.findOne(orderTO.getAreaId());
		if (areaDO == null)
			throw new AreaNotFoundException(" Area not found id- "+ orderTO.getAreaId());
		
		OperationDO operationDO = operationRepository
				.findOne(orderTO.getOperationId());
		if (operationDO == null)
			throw new OperationNotFoundException("operation not found id- "
					+ orderTO.getOperationId());

		AreaOperationDO areaOperationDO = operationAreaRepository.findByAreaIdAndOperationId(orderTO.getAreaId(), orderTO.getOperationId());
		BigDecimal orderPrice;
		if (areaOperationDO == null) {
			orderPrice = operationDO.getPrice()
					.add(operationDO.getDefaultDltCharge())
					.add(operationDO.getDefaultWage());
		} else {
			orderPrice = operationDO.getPrice()
					.add(areaOperationDO.getDltCharge())
					.add(areaOperationDO.getWage());
		}

		if (operationDO.getOperationCode().equals("TXE")
				&& orderPrice.compareTo(new BigDecimal("1000")) == 1) {
			orderPrice = new BigDecimal("0");
		} else if (orderPrice.compareTo(new BigDecimal("2000")) == 1) {
			BigDecimal percentage = CommonUtil.percentage(orderPrice,
					new BigDecimal("5"));
			orderPrice = orderPrice.subtract(percentage);
		}
		String orderNo = CommonUtil.generateOrderNo();

		OrderDO orderDO = new OrderDO();
		orderDO.setCustomerCode(orderTO.getCustomerCode());
		orderDO.setOperation(operationDO);
		orderDO.setPrice(orderPrice);
		orderDO.setOrderNos(orderNo);
		orderDO.setArea(areaDO);
		orderRepository.save(orderDO);
		return new OrderResponseTO(orderNo, orderPrice);
	}

}

package com.app.service;

import java.util.List;

import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;
import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.ReceiptNotFoundException;
import com.app.to.CreateOperationAreaTO;
import com.app.to.CreateOperationTO;
import com.app.to.OperationAreaTO;
import com.app.to.OperationTO;

public interface OperationService {


	OperationDO createOperation(CreateOperationTO operationTO) throws ReceiptNotFoundException;

	List<OperationTO> findAllOperations();

	List<OperationAreaTO> findAllOperationAreas();

	AreaOperationDO createOperationArea(CreateOperationAreaTO operationAreaTO)
			throws OperationNotFoundException, AreaNotFoundException;
	
}

package com.app.service;

import java.util.List;

import org.springframework.data.domain.Sort;

import com.app.exception.ReceiptNotFoundException;
import com.app.to.ReceiptCodeTO;

public interface ReceiptService {
	List<ReceiptCodeTO> retrieveAllReceipts(Integer page, Integer size,
			Sort.Direction direction, String sortProperty);

	ReceiptCodeTO retrieveReceiptsById(Long id) throws ReceiptNotFoundException;

	void createReceipt(ReceiptCodeTO receiptCodeTO);

	void updateReceipt(ReceiptCodeTO receiptCodeTO)
			throws ReceiptNotFoundException;

	void deleteReceiptById(Long id) throws ReceiptNotFoundException;

	List<ReceiptCodeTO> retrieveAllReceipts();

}

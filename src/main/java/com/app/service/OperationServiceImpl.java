package com.app.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.AreaRepository;
import com.app.dao.OperationAreaRepository;
import com.app.dao.OperationRepository;
import com.app.dao.ReceiptCodeRepository;
import com.app.dao.model.AreaDO;
import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;
import com.app.dao.model.ReceiptCodeDO;
import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.ReceiptNotFoundException;
import com.app.to.CreateOperationAreaTO;
import com.app.to.CreateOperationTO;
import com.app.to.OperationAreaTO;
import com.app.to.OperationTO;


@Service
public class OperationServiceImpl implements OperationService {

	@Autowired
	private OperationRepository operationRepository;
	
	@Autowired
	private OperationAreaRepository operationAreaRepository;

	@Autowired
	private ReceiptCodeRepository receiptCodeRepository;
	
	@Autowired
	private AreaRepository areaRepository;
	
	@Override
	public List<OperationTO> findAllOperations(){
		return operationRepository.findAll().stream().map( op ->new OperationTO(op)).collect(Collectors.toList());
	}
	
	@Override
	public List<OperationAreaTO> findAllOperationAreas(){
		return operationAreaRepository.findAll().stream().map( op ->new OperationAreaTO(op)).collect(Collectors.toList());
	}
	
	@Override
	public  OperationDO createOperation(CreateOperationTO operationTO) throws ReceiptNotFoundException {
		ReceiptCodeDO receiptCode = receiptCodeRepository.findOne(operationTO.getReceiptCodeId());
		if (receiptCode == null)
			throw new ReceiptNotFoundException("receipt not found id- " + operationTO.getReceiptCodeId());
		OperationDO operationDO = new OperationDO();
		operationDO.setOperationCode(operationTO.getOperationCode());
		operationDO.setOperationDesc(operationTO.getOperationDesc());
		operationDO.setPrice(operationTO.getPrice());
		operationDO.setDefaultDltCharge(operationTO.getDefaultDltCharge());
		operationDO.setDefaultWage(operationTO.getDefaultWage());
		operationDO.setReceiptCode(receiptCode);
		return operationRepository.save(operationDO);
	}
	
	

	
	@Override
	public  AreaOperationDO createOperationArea(CreateOperationAreaTO operationAreaTO) throws OperationNotFoundException, AreaNotFoundException {
		AreaDO areaDO = areaRepository.findOne(operationAreaTO.getAreaId());
		if (areaDO == null)
			throw new AreaNotFoundException(" Area not found id- "+ operationAreaTO.getAreaId());
		OperationDO operationDO = operationRepository.findOne(operationAreaTO.getOperationId());
		if (operationDO == null)
			throw new OperationNotFoundException(" operation not found id- "+ operationAreaTO.getOperationId());
		
		AreaOperationDO areaOperationDO = operationAreaRepository.findByAreaIdAndOperationId(operationAreaTO.getAreaId(), operationAreaTO.getOperationId());
		
		if (areaOperationDO == null){
			areaOperationDO = new AreaOperationDO(); 
			areaOperationDO.setArea(areaDO);
			areaOperationDO.setDltCharge(operationAreaTO.getDltCharge());
			areaOperationDO.setWage(operationAreaTO.getWage());
			areaOperationDO.setOperation(operationDO);
		} else{
			areaOperationDO.setDltCharge(operationAreaTO.getDltCharge());
			areaOperationDO.setWage(operationAreaTO.getWage());
		}
		
		
		return operationAreaRepository.save(areaOperationDO);
	}

}

package com.app.service;

import java.util.List;

import org.springframework.data.domain.Sort;

import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.ReceiptNotFoundException;
import com.app.to.AreaTO;
import com.app.to.OperationAreaTO;
import com.app.to.OperationTO;
import com.app.to.OrderResponseTO;
import com.app.to.CreateOrderTO;
import com.app.to.OrderTO;
import com.app.to.ReceiptCodeTO;

public interface OrderService {

	OrderResponseTO createOrder(CreateOrderTO orderTO) throws AreaNotFoundException, OperationNotFoundException;

	List<OrderTO> findAllOrders();

	
	
}

package com.app.to;


public class CreateOrderTO {

	private Long operationId;

	private Long areaId;

private String customerCode;


	
	public Long getOperationId() {
	return operationId;
}



public void setOperationId(Long operationId) {
	this.operationId = operationId;
}



public Long getAreaId() {
	return areaId;
}



public void setAreaId(Long areaId) {
	this.areaId = areaId;
}



public String getCustomerCode() {
	return customerCode;
}



public void setCustomerCode(String customerCode) {
	this.customerCode = customerCode;
}



	public CreateOrderTO() {
		super();
		// TODO Auto-generated constructor stub
	}



	public CreateOrderTO(Long operationId, Long areaId) {
		super();
		this.operationId = operationId;
		this.areaId = areaId;
	}

	
}

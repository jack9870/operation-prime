package com.app.to;

import java.math.BigDecimal;

import com.app.dao.model.OrderDO;

public class OrderTO {

	private String customerCode;
	
	private String operationName;
	
	private String orderNo;
	
	private BigDecimal price;
	

	
	
	public OrderTO(OrderDO orderDO ) {
		super();
		this.customerCode = orderDO.getCustomerCode();
		this.orderNo = orderDO.getOrderNos();
		this.operationName = orderDO.getOperation().getOperationDesc();
		
		this.price = orderDO.getPrice();
		
		
	}

	
	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public OrderTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	
}

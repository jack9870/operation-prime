package com.app.to;

import java.math.BigDecimal;

import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;


public class CreateOperationAreaTO {

	private Long operationId;
	
	private Long  areaId;
	
	private BigDecimal dltCharge;

	private BigDecimal wage;
	
	

	public CreateOperationAreaTO(Long operationId, Long areaId) {
		super();
		this.operationId = operationId;
		this.areaId = areaId;
	}



	public CreateOperationAreaTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Long getOperationId() {
		return operationId;
	}



	public void setOperationId(Long operationId) {
		this.operationId = operationId;
	}



	public Long getAreaId() {
		return areaId;
	}



	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}



	public BigDecimal getDltCharge() {
		return dltCharge;
	}

	public void setDltCharge(BigDecimal dltCharge) {
		this.dltCharge = dltCharge;
	}

	public BigDecimal getWage() {
		return wage;
	}

	public void setWage(BigDecimal wage) {
		this.wage = wage;
	}


	
	
	
	

}

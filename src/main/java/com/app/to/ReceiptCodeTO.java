package com.app.to;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.app.dao.model.ReceiptCodeDO;

public class ReceiptCodeTO {

	private Long id;

	private String receiptCode;

	
	public ReceiptCodeTO(ReceiptCodeDO receiptCodeDO) {
		super();
		this.id = receiptCodeDO.getId();
		this.receiptCode = receiptCodeDO.getReceiptCode();
	}

	public ReceiptCodeTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReceiptCode() {
		return receiptCode;
	}

	public void setReceiptCode(String receiptCode) {
		this.receiptCode = receiptCode;
	}

}

package com.app.to;

import java.math.BigDecimal;

import com.app.dao.model.OperationDO;
import com.app.dao.model.ReceiptCodeDO;

public class CreateOperationTO {


	private String operationCode;
	
	private String operationDesc;
	
	
	private Long receiptCodeId;
	
	private BigDecimal price;
	
	private BigDecimal defaultDltCharge;
	
	private BigDecimal defaultWage;
	

	public CreateOperationTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	



	






	public CreateOperationTO(String operationCode, String operationDesc,
			Long receiptCodeId) {
		super();
		this.operationCode = operationCode;
		this.operationDesc = operationDesc;
		this.receiptCodeId = receiptCodeId;
	}













	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getOperationDesc() {
		return operationDesc;
	}

	public void setOperationDesc(String operationDesc) {
		this.operationDesc = operationDesc;
	}

	public Long getReceiptCodeId() {
		return receiptCodeId;
	}

	public void setReceiptCodeId(Long receiptCodeId) {
		this.receiptCodeId = receiptCodeId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDefaultDltCharge() {
		return defaultDltCharge;
	}

	public void setDefaultDltCharge(BigDecimal defaultDltCharge) {
		this.defaultDltCharge = defaultDltCharge;
	}


	public BigDecimal getDefaultWage() {
		return defaultWage;
	}


	public void setDefaultWage(BigDecimal defaultWage) {
		this.defaultWage = defaultWage;
	}

	
	

}

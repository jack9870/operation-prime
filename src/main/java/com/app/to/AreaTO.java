package com.app.to;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.app.dao.model.AreaDO;


public class AreaTO {
	
	
	
	public AreaTO(AreaDO areaDO) {
		super();
		this.id = areaDO.getId();
		this.provinceArea = areaDO.getProvinceArea();
	
	}
	
	

	public AreaTO(Long id, String provinceArea) {
		super();
		this.id = id;
		this.provinceArea = provinceArea;
	}



	public AreaTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private Long id;
	
	
	private String provinceArea;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProvinceArea() {
		return provinceArea;
	}

	public void setProvinceArea(String provinceArea) {
		this.provinceArea = provinceArea;
	}

	
	
	

}

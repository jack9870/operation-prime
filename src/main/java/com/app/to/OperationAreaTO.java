package com.app.to;

import java.math.BigDecimal;

import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;


public class OperationAreaTO {


	
	private String operation;
	
	private String  area;
	
	private BigDecimal dltCharge;

	private BigDecimal wage;

	public OperationAreaTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	
	public OperationAreaTO(AreaOperationDO areaOperationDO){
		this.operation = areaOperationDO.getOperation().getOperationDesc();
		this.area = areaOperationDO.getArea().getProvinceArea();
		this.dltCharge = areaOperationDO.getDltCharge();
		this.wage = areaOperationDO.getWage();
	}




	public String getOperation() {
		return operation;
	}




	public void setOperation(String operation) {
		this.operation = operation;
	}




	public String getArea() {
		return area;
	}




	public void setArea(String area) {
		this.area = area;
	}




	public BigDecimal getDltCharge() {
		return dltCharge;
	}

	public void setDltCharge(BigDecimal dltCharge) {
		this.dltCharge = dltCharge;
	}

	public BigDecimal getWage() {
		return wage;
	}

	public void setWage(BigDecimal wage) {
		this.wage = wage;
	}


	
	

}

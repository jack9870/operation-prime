package com.app.to;

import java.math.BigDecimal;

public class OrderResponseTO {
	
	
	

	public OrderResponseTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	




	public OrderResponseTO(String orderNo, BigDecimal totalPrice) {
		super();
		this.orderNo = orderNo;
		this.totalPrice = totalPrice;
	}





	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}





	private String orderNo;
	
	private BigDecimal totalPrice;

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	
	
}

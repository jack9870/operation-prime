package com.app.to;

import java.math.BigDecimal;

import com.app.dao.model.OperationDO;
import com.app.dao.model.ReceiptCodeDO;

public class OperationTO {

	private Long operationId;
	private String operationCode;
	
	private String operationDesc;
	
	private String receiptCode;
	
	private Long receiptCodeId;
	
	private BigDecimal price;
	
	private BigDecimal defaultDltCharge;
	
	private BigDecimal defaultWage;
	

	public OperationTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public OperationTO(OperationDO operationDO) {
		super();
		this.operationId = operationDO.getId();
		this.operationCode = operationDO.getOperationCode();
		this.operationDesc = operationDO.getOperationDesc();
		this.receiptCode = operationDO.getReceiptCode().getId() +" "+operationDO.getReceiptCode().getReceiptCode();
		this.price = operationDO.getPrice();
		this.defaultDltCharge = operationDO.getDefaultDltCharge();
		this.defaultWage = operationDO.getDefaultWage();
	}

	

	public Long getOperationId() {
		return operationId;
	}


	public void setOperationId(Long operationId) {
		this.operationId = operationId;
	}


	public String getReceiptCode() {
		return receiptCode;
	}


	public void setReceiptCode(String receiptCode) {
		this.receiptCode = receiptCode;
	}


	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getOperationDesc() {
		return operationDesc;
	}

	public void setOperationDesc(String operationDesc) {
		this.operationDesc = operationDesc;
	}

	public Long getReceiptCodeId() {
		return receiptCodeId;
	}

	public void setReceiptCodeId(Long receiptCodeId) {
		this.receiptCodeId = receiptCodeId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDefaultDltCharge() {
		return defaultDltCharge;
	}

	public void setDefaultDltCharge(BigDecimal defaultDltCharge) {
		this.defaultDltCharge = defaultDltCharge;
	}


	public BigDecimal getDefaultWage() {
		return defaultWage;
	}


	public void setDefaultWage(BigDecimal defaultWage) {
		this.defaultWage = defaultWage;
	}

	
	

}

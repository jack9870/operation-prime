package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.dao.model.AreaDO;
import com.app.dao.model.OperationDO;
import com.app.dao.model.OrderDO;
import com.app.dao.model.ReceiptCodeDO;

public interface OrderRepository extends JpaRepository<OrderDO, Long> {
}

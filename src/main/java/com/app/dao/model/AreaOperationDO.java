package com.app.dao.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "area_operation")
public class AreaOperationDO {

	public AreaOperationDO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "dlt_charge")
	private BigDecimal dltCharge = new BigDecimal("0.00");

	@Column(name = "wage")
	private BigDecimal wage = new BigDecimal("0.00");

	@ManyToOne
	private AreaDO area;

	@ManyToOne
	private OperationDO operation;

	
	
	public AreaOperationDO(Long id, AreaDO area, OperationDO operation) {
		super();
		this.id = id;
		this.area = area;
		this.operation = operation;
	}
	
	

	public AreaOperationDO(Long id, 
			AreaDO area, OperationDO operation, BigDecimal dltCharge, BigDecimal wage) {
		super();
		this.id = id;
		this.dltCharge = dltCharge;
		this.wage = wage;
		this.area = area;
		this.operation = operation;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getDltCharge() {
		return dltCharge;
	}

	public void setDltCharge(BigDecimal dltCharge) {
		this.dltCharge = dltCharge;
	}

	public BigDecimal getWage() {
		return wage;
	}

	public void setWage(BigDecimal wage) {
		this.wage = wage;
	}

	public OperationDO getOperation() {
		return operation;
	}

	public void setOperation(OperationDO operation) {
		this.operation = operation;
	}

	public AreaDO getArea() {
		return area;
	}

	public void setArea(AreaDO area) {
		this.area = area;
	}


}

package com.app.dao.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.app.to.AreaTO;

@Entity
@Table(name = "area")
public class AreaDO {
	
	
	
	public AreaDO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public AreaDO(Long id, String provinceArea) {
		super();
		this.id = id;
		this.provinceArea = provinceArea;
	}


	public AreaDO(AreaTO areaTO) {
		super();
		
		this.provinceArea = areaTO.getProvinceArea();
		
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="province_area")
	private String provinceArea;
	
	@OneToMany(mappedBy = "area", cascade = CascadeType.ALL, orphanRemoval = true)
	 private List<AreaOperationDO> areaOperations = new ArrayList<AreaOperationDO>();
	
	@OneToMany(mappedBy = "area", cascade = CascadeType.ALL, orphanRemoval = true)
	 private List<OrderDO> orders= new ArrayList<OrderDO>();
	
	

	public List<OrderDO> getOrders() {
		return orders;
	}


	public void setOrders(List<OrderDO> orders) {
		this.orders = orders;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProvinceArea() {
		return provinceArea;
	}

	public void setProvinceArea(String provinceArea) {
		this.provinceArea = provinceArea;
	}

	public List<AreaOperationDO> getAreaOperations() {
		return areaOperations;
	}

	public void setAreaOperations(List<AreaOperationDO> areaOperations) {
		this.areaOperations = areaOperations;
	}

	
	
	
	

}

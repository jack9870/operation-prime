package com.app.dao.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.app.to.ReceiptCodeTO;

@Entity
@Table(name = "receipt")
public class ReceiptCodeDO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="receipt_code")
	private String receiptCode;
	
	@OneToMany(mappedBy = "receiptCode", cascade = CascadeType.ALL, orphanRemoval = true)
	 private List<OperationDO> operations = new ArrayList<OperationDO>();
	

	public ReceiptCodeDO(ReceiptCodeTO receiptCodeTO) {
		super();
		this.receiptCode = receiptCodeTO.getReceiptCode();
	}

	public ReceiptCodeDO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ReceiptCodeDO(Long id, String receiptCode) {
		super();
		this.id = id;
		this.receiptCode = receiptCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReceiptCode() {
		return receiptCode;
	}

	public void setReceiptCode(String receiptCode) {
		this.receiptCode = receiptCode;
	}

	public List<OperationDO> getOperations() {
		return operations;
	}

	public void setOperations(List<OperationDO> operations) {
		this.operations = operations;
	}

	

	
	

}

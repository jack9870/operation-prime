package com.app.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "operation")
public class OperationDO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "operation_code")
	private String operationCode;

	@Column(name = "operation_desc")
	private String operationDesc;

	@ManyToOne
	private ReceiptCodeDO receiptCode;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "area_operation_id")
	private AreaOperationDO areaOperation;

	@Column(name = "price")
	private BigDecimal price= new BigDecimal("0.00");

	@Column(name = "default_dlt_charge")
	private BigDecimal defaultDltCharge= new BigDecimal("0.00");

	@Column(name = "default_wage")
	private BigDecimal defaultWage= new BigDecimal("0.00");
	
	@OneToMany(mappedBy = "operation", cascade = CascadeType.ALL, orphanRemoval = true)
	 private List<OrderDO> orders= new ArrayList<OrderDO>();
	
	@OneToMany(mappedBy = "operation", cascade = CascadeType.ALL, orphanRemoval = true)
	 private List<AreaOperationDO> areaOperations = new ArrayList<AreaOperationDO>();
	
	
	public OperationDO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OperationDO(Long id, String operationCode, String operationDesc) {
		super();
		this.id = id;
		this.operationCode = operationCode;
		this.operationDesc = operationDesc;
	}
	
	

	public OperationDO(Long id, String operationCode, String operationDesc,
			BigDecimal price, BigDecimal defaultDltCharge,
			BigDecimal defaultWage) {
		super();
		this.id = id;
		this.operationCode = operationCode;
		this.operationDesc = operationDesc;
		this.price = price;
		this.defaultDltCharge = defaultDltCharge;
		this.defaultWage = defaultWage;
	}

	public List<AreaOperationDO> getAreaOperations() {
		return areaOperations;
	}

	public void setAreaOperations(List<AreaOperationDO> areaOperations) {
		this.areaOperations = areaOperations;
	}

	public List<OrderDO> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderDO> orders) {
		this.orders = orders;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getOperationDesc() {
		return operationDesc;
	}

	public void setOperationDesc(String operationDesc) {
		this.operationDesc = operationDesc;
	}

	public ReceiptCodeDO getReceiptCode() {
		return receiptCode;
	}

	public void setReceiptCode(ReceiptCodeDO receiptCode) {
		this.receiptCode = receiptCode;
	}

	public AreaOperationDO getAreaOperation() {
		return areaOperation;
	}

	public void setAreaOperation(AreaOperationDO areaOperation) {
		this.areaOperation = areaOperation;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDefaultDltCharge() {
		return defaultDltCharge;
	}

	public void setDefaultDltCharge(BigDecimal defaultDltCharge) {
		this.defaultDltCharge = defaultDltCharge;
	}

	public BigDecimal getDefaultWage() {
		return defaultWage;
	}

	public void setDefaultWage(BigDecimal defaultWage) {
		this.defaultWage = defaultWage;
	}

	
}

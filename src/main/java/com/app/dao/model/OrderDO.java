package com.app.dao.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer_order")
public class OrderDO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	private OperationDO operation;
	
	@ManyToOne
	private AreaDO area;
	

	@Column(name = "order_no")
	private String orderNos;
	
	@Column(name = "customer_code")
	private String customerCode;
	
	@Column(name = "price")
	private BigDecimal price= new BigDecimal("0.00");
	
	
	
	
	

	public OrderDO(
			String orderNos, String customerCode, BigDecimal price) {
		super();
		this.orderNos = orderNos;
		this.customerCode = customerCode;
		this.price = price;
	}

	public OrderDO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AreaDO getArea() {
		return area;
	}

	public void setArea(AreaDO area) {
		this.area = area;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public OperationDO getOperation() {
		return operation;
	}

	public void setOperation(OperationDO operation) {
		this.operation = operation;
	}

	public String getOrderNos() {
		return orderNos;
	}

	public void setOrderNos(String orderNos) {
		this.orderNos = orderNos;
	}

	

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


}

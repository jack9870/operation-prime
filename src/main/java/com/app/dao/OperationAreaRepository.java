package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.dao.model.AreaOperationDO;

public interface OperationAreaRepository extends JpaRepository<AreaOperationDO, Long> {
	
	AreaOperationDO findByAreaIdAndOperationId(Long areaId, Long operationId);
}

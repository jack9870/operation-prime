package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.dao.model.AreaDO;

public interface AreaRepository extends JpaRepository<AreaDO, Long> {

}

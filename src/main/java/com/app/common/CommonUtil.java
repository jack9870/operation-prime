package com.app.common;

import java.math.BigDecimal;
import java.util.UUID;

public class CommonUtil {

	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

	public static BigDecimal percentage(BigDecimal base, BigDecimal pct){
	    return base.multiply(pct).divide(ONE_HUNDRED);
	}
	
	public static String generateOrderNo() {
		return UUID.randomUUID().toString();
	}
}

package com.app.rest;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.exception.ReceiptNotFoundException;
import com.app.service.ReceiptService;
import com.app.to.ReceiptCodeTO;

@RestController
public class ReceiptCodeController {

	@Autowired
	private ReceiptService receiptService;
	
	@RequestMapping(value="/receipt/{id}", method = RequestMethod.GET)
	public ReceiptCodeTO retrieveByArea(@PathVariable Long id) throws ReceiptNotFoundException {
		return receiptService.retrieveReceiptsById(id);
	}
	
	@RequestMapping(value="/receipts", method = RequestMethod.GET)
	public List<ReceiptCodeTO> retrieveAllAreas(
	        @RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
	        @RequestParam(value = "count",required = false) Integer size,
	        @RequestParam(value = "order", defaultValue = "ASC", required = false) Sort.Direction direction,
	        @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {
		return receiptService.retrieveAllReceipts(page, size, direction, sortProperty);
	}
	
	@RequestMapping(value="/receipt", method = RequestMethod.POST)
	public ResponseEntity<Void> createArea(@RequestBody ReceiptCodeTO  receiptCodeTO) {
		 receiptService.createReceipt(receiptCodeTO);
		 return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value="/receipt", method = RequestMethod.PUT)
	public void retrieveByArea(@RequestBody ReceiptCodeTO  receiptCodeTO) throws ReceiptNotFoundException {
		 receiptService.updateReceipt(receiptCodeTO);
	}

	
	@RequestMapping(value="/receipt/{id}", method = RequestMethod.DELETE)
	public void deleteAreaById(@PathVariable Long id) throws ReceiptNotFoundException {
		 receiptService.deleteReceiptById(id);
	}
	
}

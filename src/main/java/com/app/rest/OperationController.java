package com.app.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.ReceiptNotFoundException;
import com.app.service.AreaService;
import com.app.service.OperationService;
import com.app.service.ReceiptService;
import com.app.to.CreateOperationAreaTO;
import com.app.to.CreateOperationTO;
import com.app.to.OperationAreaTO;
import com.app.to.OperationTO;

@RestController
public class OperationController {

	@Autowired
	private OperationService operationService;
	
	@Autowired
	private ReceiptService receiptService;
	
	@Autowired
	private AreaService areaService;

	@RequestMapping(value = "/operation", method = RequestMethod.POST)
	public ModelAndView createArea(@ModelAttribute("operationTO")  CreateOperationTO operationTO) {
		

		ModelAndView model = new ModelAndView("redirect:/operations");
		if (operationTO != null) {
			try {
				operationService.createOperation(operationTO);
			} catch (ReceiptNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				model.addObject("danger", "Something Going Bad");
			}
			model.addObject("warning", "User operation Success");

		} else {
			model.addObject("danger", "Something Going Bad");

		}
		return model;

	}

	@RequestMapping(value = "/operation-area", method = RequestMethod.POST)
	public ModelAndView createOperationArea(@ModelAttribute("operationAreaTO") CreateOperationAreaTO operationAreaTO) {
		
		
		ModelAndView model = new ModelAndView("redirect:/operation-areas");
		if (operationAreaTO != null) {
				try {
					operationService.createOperationArea(operationAreaTO);
				} catch (OperationNotFoundException | AreaNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			model.addObject("warning", "User area operation Success");

		} else {
			model.addObject("danger", "Something Going Bad");

		}
		return model;
		
	}

	@RequestMapping(value = "/operations", method = RequestMethod.GET)
	public ModelAndView operations() {
		ModelAndView model = new ModelAndView("operations");
		model.addObject("receiptList", receiptService.retrieveAllReceipts());
		model.addObject("list", operationService.findAllOperations());
		return model;
	}
	
	@RequestMapping(value = "/operation-areas", method = RequestMethod.GET)
	public ModelAndView operationAreas() {
		ModelAndView model = new ModelAndView("operation-areas");
		model.addObject("areaList", areaService.retrieveAllAreas());
		model.addObject("oplist", operationService.findAllOperations());
		model.addObject("list", operationService.findAllOperationAreas());
		return model;
	}

	
}

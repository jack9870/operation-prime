package com.app.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.OrderNotFoundException;
import com.app.service.AreaService;
import com.app.service.OperationService;
import com.app.service.OrderService;
import com.app.to.CreateOrderTO;

@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private AreaService areaService;

	@Autowired
	private OperationService operationService;

	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ModelAndView createOrder(@ModelAttribute("orderTO") CreateOrderTO orderTO)
			throws OrderNotFoundException, AreaNotFoundException,
			OperationNotFoundException {

		ModelAndView model = new ModelAndView("redirect:/orders");
		if (orderTO != null) {

			orderService.createOrder(orderTO);

			model.addObject("warning", " Order creation Success");

		} else {
			model.addObject("danger", "Something Going Bad");

		}
		return model;

	}

	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ModelAndView operations() {
		ModelAndView model = new ModelAndView("orders");
		model.addObject("areaList", areaService.retrieveAllAreas());
		model.addObject("oplist", operationService.findAllOperations());
		model.addObject("orlist", orderService.findAllOrders());
		return model;
	}

}

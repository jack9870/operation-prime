package com.app.rest;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.exception.AreaNotFoundException;
import com.app.service.AreaService;
import com.app.to.AreaTO;

@RestController
public class AreaController {

	@Autowired
	private AreaService areaService;
	
	@RequestMapping(value="/area/{id}", method = RequestMethod.GET)
	
	public AreaTO retrieveByArea(@PathVariable Long id) throws AreaNotFoundException {
		return areaService.retrieveAreaById(id);
	}
	
	
	@RequestMapping(value="/areas", method = RequestMethod.GET)
	public List<AreaTO> retrieveAllAreas(
	        @RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
	        @RequestParam(value = "count",required = false) Integer size,
	        @RequestParam(value = "order", defaultValue = "ASC", required = false) Sort.Direction direction,
	        @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty) {
		return areaService.retrieveAllAreas(page, size, direction, sortProperty);
	}
	
	
	@RequestMapping(value="/area", method = RequestMethod.POST)
	public ResponseEntity<Void> createArea(@RequestBody AreaTO  areaTO) throws AreaNotFoundException {
		 areaService.createArea(areaTO);
		 return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	
	
	@RequestMapping(value="/area", method = RequestMethod.PUT)
	public void updateArea(@RequestBody AreaTO  areaTO) throws AreaNotFoundException {
		 areaService.updateArea(areaTO);
		 
	}

	
	@RequestMapping(value="/area/{id}", method = RequestMethod.DELETE)
	public void deleteAreaById(@PathVariable Long id) throws AreaNotFoundException {
		 areaService.deleteAreaById(id);
		 
	}
	
}

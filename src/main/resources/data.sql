insert into area(id, province_area) values (1, 'bangkok')
insert into area(id, province_area) values (2, 'pattaya')
insert into receipt(id, receipt_code) values (1, 'pay insurance')
insert into receipt(id, receipt_code) values (2, 'pay servicing')
insert into operation(id, operation_code, operation_desc,receipt_code_id, price, default_dlt_charge, default_wage) values (1,'TXE', 'Tax Extension',1, 500,  0, 0)

insert into operation(id, operation_code, operation_desc,receipt_code_id, price, default_dlt_charge, default_wage) values (2,'SR', 'Servicing',2, 1200,  100, 200)
insert into area_operation(id, dlt_charge, wage,area_id, operation_id) values (1,300, 300,1, 1)
insert into area_operation(id, dlt_charge, wage,area_id, operation_id) values (2,500, 500,2, 2)
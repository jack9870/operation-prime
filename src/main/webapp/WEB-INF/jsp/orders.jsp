<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="header.jsp"></jsp:include>

<h3>Order</h3>
<br>
<form action='/order' method='post'>

	<table class='table table-hover table-responsive table-bordered'>

		<tr>
			<td><b>Operation * </b></td>
			<td><select id="operationId" name="operationId">
                        
                        <c:forEach items="${oplist}" var="op">
                            <option   value="${op.operationId}" >${op.operationDesc}</option>
                        </c:forEach>
                    </select></td>
		</tr>

		
		<tr>
			<td><b>Province Area * </b></td>
			<td><select id="areaId" name="areaId">
                        
                        <c:forEach items="${areaList}" var="area">
                            <option   value="${area.id}" >${area.provinceArea}</option>
                        </c:forEach>
                    </select></td>
		</tr>
		<tr>
			<td><b>Customer Code * </b></td>
			<td><input type='text' name='customerCode' class='form-control'
				required /></td>
		</tr>
		

		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">Create New Order</button>
			</td>
		</tr>

	</table>
	<b><c:out value="${danger}"></c:out></b>
</form>



<h3>List Of Orders</h3>
<br>
<table class="table table-hover">

	<thead>
		<tr>
			<th><b>Order No</b></th>
			<th><b>Operation Description</b></th>
			<th><b>Price</b></th>
			<th><b>Customer Code</b></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${orlist}" var="lou">
			<tr>
				<td><c:out value="${lou.orderNo}"></c:out></td>
				<td><c:out value="${lou.operationName}"></c:out></td>
				<td><c:out value="${lou.price}"></c:out></td>
				<td><c:out value="${lou.customerCode}"></c:out></td>
			</tr>

		</c:forEach>
	</tbody>
</table>
</div>

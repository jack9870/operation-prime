<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="header.jsp"></jsp:include>

<h3>Operation</h3>
<br>
<form action='/operation' method='post'>

	<table class='table table-hover table-responsive table-bordered'>

		<tr>
			<td><b>Operation Code * </b></td>
			<td><input type='text' name='operationCode' class='form-control'
				required /></td>
		</tr>

		<tr>
			<td><b>Operation Description * </b></td>
			<td><input type='text' name='operationDesc' class='form-control'
				required /></td>
		</tr>
		<tr>
			<td><b>Receipt Code * </b></td>
			<td><select id = "receiptCodeId" name="receiptCodeId">
                        <c:forEach items="${receiptList}" var="rec">
                            <option   value="${rec.id}" >${rec.receiptCode}</option>
                        </c:forEach>
                    </select></td>
		</tr>

		<tr>
			<td><b>Price</b></td>
			<td><input type='text' name='price' class='form-control'
				required /></td>

		</tr>
		<tr>
			<td><b>Default DLT Charge  (Enter Number only) *</b></td>
			<td><input type='text' name='defaultDltCharge'
				class='form-control' required /></td>

		</tr>
		<tr>
			<td><b>Default Wage  (Enter Number only) *</b></td>
			<td><input type='text' name='defaultWage' class='form-control'
				required /></td>

		</tr>

		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">Create New Operation</button>
			</td>
		</tr>

	</table>
	<b><c:out value="${danger}"></c:out></b>
</form>



<h3>List Of Operations</h3>
<br>
<table class="table table-hover">

	<thead>
		<tr>
			<th><b>Operation Code</b></th>
			<th><b>Operation Description</b></th>
			<th><b>Receipt Code</b></th>
			<th><b>Price</b></th>
			<th><b>Default DLT Charge</b></th>
			<th><b>Default Wage</b></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list}" var="lou">
			<tr>
				<td><c:out value="${lou.operationCode}"></c:out></td>
				<td><c:out value="${lou.operationDesc}"></c:out></td>
				<td><c:out value="${lou.receiptCode}"></c:out></td>
				<td><c:out value="${lou.price}"></c:out></td>
				<td><c:out value="${lou.defaultDltCharge}"></c:out></td>
				<td><c:out value="${lou.defaultWage}"></c:out></td>
			</tr>

		</c:forEach>
	</tbody>
</table>
</div>

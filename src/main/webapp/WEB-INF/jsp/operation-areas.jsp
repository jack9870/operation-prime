<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:include page="header.jsp"></jsp:include>

<h3>Operation Area</h3>
<br>
<form action='/operation-area' method='post'>

	<table class='table table-hover table-responsive table-bordered'>

	<tr>
			<td><b>Operation *</b></td>
			<td><select id="operationId" name="operationId">
                        
                        <c:forEach items="${oplist}" var="op">
                            <option   value="${op.operationId}" >${op.operationDesc}</option>
                        </c:forEach>
                    </select></td>
		</tr>

		<tr>
			<td><b>Province Area *</b></td>
			<td><select id="areaId" name="areaId">
                        
                        <c:forEach items="${areaList}" var="area">
                            <option   value="${area.id}" >${area.provinceArea}</option>
                        </c:forEach>
                    </select></td>
		</tr>

		<tr>
			<td><b>DLT Charge  (Enter Number only) *</b></td>
			<td><input type='text' name='dltCharge'
				class='form-control' required /></td>

		</tr>
		<tr>
			<td><b>Wage (Enter Number only) *</b></td>
			<td><input type='text' name='wage' class='form-control'
				required /></td>

		</tr>

		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">Update Operation Area</button>
			</td>
		</tr>

	</table>
	<b><c:out value="${danger}"></c:out></b>
</form>



<h3>List Of Operation Areas</h3>
<br>
<table class="table table-hover">

	<thead>
		<tr>
		    <th><b>Operation  </b></th>
			<th><b>Province Area </b></th>
			<th><b>DLT Charge</b></th>
			<th><b>Wage </b></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list}" var="lou">
			<tr>
				<td><c:out value="${lou.operation}"></c:out></td>
				<td><c:out value="${lou.area}"></c:out></td>
				<td><c:out value="${lou.dltCharge}"></c:out></td>
				<td><c:out value="${lou.wage}"></c:out></td>
			</tr>

		</c:forEach>
	</tbody>
</table>
</div>

package com.app.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.Application;
import com.app.dao.AreaRepository;
import com.app.dao.OperationAreaRepository;
import com.app.dao.OperationRepository;
import com.app.dao.ReceiptCodeRepository;
import com.app.dao.model.AreaDO;
import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;
import com.app.dao.model.ReceiptCodeDO;
import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.ReceiptNotFoundException;
import com.app.to.CreateOperationAreaTO;
import com.app.to.CreateOperationTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class OperationServiceTest {

	@Mock
	private AreaRepository areaRepository;
	
	@Mock
	private ReceiptCodeRepository codeRepository;
	
	@Mock
	private OperationRepository operationRepository;

	@Mock
	private OperationAreaRepository operationAreaRepository;
	
	@Before
	public void setUp() {
	    MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	private OperationServiceImpl operationServiceImpl;
	
	
	
	

	@Test
	public void testCreateOperation() throws AreaNotFoundException, ReceiptNotFoundException {
		// given
		ReceiptCodeDO receiptCodeDO = new ReceiptCodeDO(1L, "pay service");
		CreateOperationTO operationTO = new CreateOperationTO("TXE", "Tax Extension", 1L);
		OperationDO operationDO = new OperationDO(1L, "TXE", "Tax Extension");
		
		// when
		when(codeRepository.findOne(1L)).thenReturn(receiptCodeDO);
		when(operationRepository.save(any(OperationDO.class))).thenReturn(operationDO);
		
		// then
		assertEquals(operationDO.getOperationCode(), operationServiceImpl.createOperation(operationTO).getOperationCode());
	}

	@Test
	public void testCreateOperationArea() throws AreaNotFoundException, ReceiptNotFoundException, OperationNotFoundException {
		// given
		AreaDO areaDO = new AreaDO(1L, "bangkok");
		OperationDO  operationDO = new OperationDO(1L, "TXE", "Tax Extension");
		AreaOperationDO  areaOperationDO = new AreaOperationDO(1L, areaDO, operationDO);
		CreateOperationAreaTO createOperationAreaTO = new CreateOperationAreaTO(1L, 1L);
		
		// when
		when(areaRepository.findOne(1L)).thenReturn(areaDO);
		
		when(operationRepository.findOne(1L)).thenReturn(operationDO);
		
		when(operationAreaRepository.findByAreaIdAndOperationId(1L, 1L)).thenReturn(areaOperationDO);
		
		when(operationAreaRepository.save(any(AreaOperationDO.class))).thenReturn(areaOperationDO);
		
		
		// then
		assertEquals(areaOperationDO.getId(), operationServiceImpl.createOperationArea(createOperationAreaTO).getId());
	}


}

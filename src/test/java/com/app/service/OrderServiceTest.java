package com.app.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.any;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.Application;
import com.app.dao.AreaRepository;
import com.app.dao.OperationAreaRepository;
import com.app.dao.OperationRepository;
import com.app.dao.OrderRepository;
import com.app.dao.ReceiptCodeRepository;
import com.app.dao.model.AreaDO;
import com.app.dao.model.AreaOperationDO;
import com.app.dao.model.OperationDO;
import com.app.dao.model.OrderDO;
import com.app.dao.model.ReceiptCodeDO;
import com.app.exception.AreaNotFoundException;
import com.app.exception.OperationNotFoundException;
import com.app.exception.ReceiptNotFoundException;
import com.app.to.CreateOperationAreaTO;
import com.app.to.CreateOperationTO;
import com.app.to.CreateOrderTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class OrderServiceTest {

	@Mock
	private AreaRepository areaRepository;
	
	@Mock
	private ReceiptCodeRepository codeRepository;
	
	@Mock
	private OperationRepository operationRepository;

	@Mock
	private OrderRepository orderRepository;
	
	@Mock
	private OperationAreaRepository operationAreaRepository;
	
	@Before
	public void setUp() {
	    MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	private OrderServiceImpl orderServiceImpl;
	
	
	
	

	@Test
	public void testCreateOrder_PriceMorethan1000AndOperationIsTaxExtension_PriceIs0() throws AreaNotFoundException, ReceiptNotFoundException, OperationNotFoundException {
		// given
				AreaDO areaDO = new AreaDO(1L, "bangkok");
				OperationDO  operationDO = new OperationDO(1L, "TXE", "Tax Extension", new BigDecimal("500"),  new BigDecimal("500"),  new BigDecimal("500"));
				AreaOperationDO  areaOperationDO = new AreaOperationDO(1L, areaDO, operationDO);
				
				OrderDO orderDO = new OrderDO("123", "51a", new BigDecimal("0"));
				CreateOrderTO orderTO = new CreateOrderTO(1L, 1L);
				
				// when
				when(areaRepository.findOne(1L)).thenReturn(areaDO);
				
				when(operationRepository.findOne(1L)).thenReturn(operationDO);
				
				when(operationAreaRepository.findByAreaIdAndOperationId(1L, 5L)).thenReturn(areaOperationDO);
				
				when(orderRepository.save(any(OrderDO.class))).thenReturn(orderDO);
				
				
		
		// then
		assertEquals(orderDO.getPrice(), orderServiceImpl.createOrder(orderTO).getTotalPrice());
	}
	
	@Test
	public void testCreateOrder_DefaultOperationWageIsUsed_PriceIs1500() throws AreaNotFoundException, ReceiptNotFoundException, OperationNotFoundException {
		// given
				AreaDO areaDO = new AreaDO(1L, "bangkok");
				OperationDO  operationDO = new OperationDO(1L, "SR", "Servicing", new BigDecimal("500"),  new BigDecimal("500"),  new BigDecimal("500"));
				AreaOperationDO  areaOperationDO = new AreaOperationDO(1L, areaDO, operationDO);
				
				OrderDO orderDO = new OrderDO("123", "51a", new BigDecimal("1500"));
				CreateOrderTO orderTO = new CreateOrderTO(1L, 1L);
				
				// when
				when(areaRepository.findOne(1L)).thenReturn(areaDO);
				
				when(operationRepository.findOne(1L)).thenReturn(operationDO);
				
				when(operationAreaRepository.findByAreaIdAndOperationId(1L, 5L)).thenReturn(areaOperationDO);
				
				when(orderRepository.save(any(OrderDO.class))).thenReturn(orderDO);
				
				
		
		// then
		assertEquals(orderDO.getPrice(), orderServiceImpl.createOrder(orderTO).getTotalPrice());
	}


	@Test
	public void test_CreateOrder_AreaSpecificWageIsUsed_PriceIs1100() throws AreaNotFoundException, ReceiptNotFoundException, OperationNotFoundException {
		// given
				AreaDO areaDO = new AreaDO(1L, "bangkok");
				OperationDO  operationDO = new OperationDO(1L, "SR", "Servicing", new BigDecimal("500"),  new BigDecimal("500"),  new BigDecimal("500"));
				AreaOperationDO  areaOperationDO = new AreaOperationDO(1L, areaDO, operationDO,  new BigDecimal("300"),  new BigDecimal("300"));
				
				OrderDO orderDO = new OrderDO("123", "51a", new BigDecimal("1100"));
				CreateOrderTO orderTO = new CreateOrderTO(1L, 1L);
				
				// when
				when(areaRepository.findOne(1L)).thenReturn(areaDO);
				
				when(operationRepository.findOne(1L)).thenReturn(operationDO);
				
				when(operationAreaRepository.findByAreaIdAndOperationId(1L, 1L)).thenReturn(areaOperationDO);
				
				when(orderRepository.save(any(OrderDO.class))).thenReturn(orderDO);
				
				
		
		// then
		assertEquals(orderDO.getPrice(), orderServiceImpl.createOrder(orderTO).getTotalPrice());
	}


	@Test
	public void testCreateOrder_TotalPriceIsGreaterThan2000Discount5Percent__PriceIs1995() throws AreaNotFoundException, ReceiptNotFoundException, OperationNotFoundException {
		// given
				AreaDO areaDO = new AreaDO(1L, "bangkok");
				OperationDO  operationDO = new OperationDO(1L, "SR", "Servicing", new BigDecimal("1500"),  new BigDecimal("500"),  new BigDecimal("500"));
				AreaOperationDO  areaOperationDO = new AreaOperationDO(1L, areaDO, operationDO,  new BigDecimal("300"),  new BigDecimal("300"));
				
				OrderDO orderDO = new OrderDO("123", "51a", new BigDecimal("1995"));
				CreateOrderTO orderTO = new CreateOrderTO(1L, 1L);
				
				// when
				when(areaRepository.findOne(1L)).thenReturn(areaDO);
				
				when(operationRepository.findOne(1L)).thenReturn(operationDO);
				
				when(operationAreaRepository.findByAreaIdAndOperationId(1L, 1L)).thenReturn(areaOperationDO);
				
				when(orderRepository.save(any(OrderDO.class))).thenReturn(orderDO);
				
				
		
		// then
		assertEquals(orderDO.getPrice(), orderServiceImpl.createOrder(orderTO).getTotalPrice());
	}




}

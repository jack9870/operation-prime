package com.app.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.Application;
import com.app.dao.AreaRepository;
import com.app.dao.ReceiptCodeRepository;
import com.app.dao.model.AreaDO;
import com.app.dao.model.ReceiptCodeDO;
import com.app.exception.AreaNotFoundException;
import com.app.exception.ReceiptNotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ReceiptTest {

	@Mock
	private ReceiptCodeRepository codeRepository;

	@Before
	public void setUp() {
	    MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	private ReceiptServiceImpl receiptServiceImpl;

	@Test
	public void testRetrieveAreaById() throws AreaNotFoundException, ReceiptNotFoundException {
		// given
		ReceiptCodeDO areaDO = new ReceiptCodeDO(1L, "pay service");
		
		// when
		when(codeRepository.findOne(1L)).thenReturn(areaDO);
		
		// then
		assertEquals(areaDO.getReceiptCode(), receiptServiceImpl.retrieveReceiptsById(1L)
				.getReceiptCode());
	}

	

}

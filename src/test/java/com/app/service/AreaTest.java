package com.app.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app.Application;
import com.app.dao.AreaRepository;
import com.app.dao.model.AreaDO;
import com.app.exception.AreaNotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class AreaTest {

	@Mock
	private AreaRepository areaRepository;

	@Before
	public void setUp() {
	    MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	private AreaServiceImpl areaService;

	@Test
	public void testRetrieveAreaById() throws AreaNotFoundException {
		// given
		AreaDO areaDO = new AreaDO(1L, "bangkok");
		
		// when
		when(areaRepository.findOne(1L)).thenReturn(areaDO);
		
		// then
		assertEquals(areaDO.getProvinceArea(), areaService.retrieveAreaById(1L)
				.getProvinceArea());
	}

	

}

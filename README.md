App name - 

Operation Prime

Description - 
This project contains the the following

Api for crud operation on Area and Receipts 

UI Screen and Api for creating and viewing Operation with default price

UI Screen and Api for creating and viewing Operation with area specific prices

UI Screen and Api for creating and viewing Order based on the area and operation selected

Which Technologies We Will Use?
Crud operations were performed using Spring Boot, H2 (Memory Based) Database and JSP / JSTL for the UI, Mockito for unit tests.


Maven Project

Step to run the project 

1. open cmd run "git clone https://jack9870@bitbucket.org/jack9870/operation-prime.git"

2. cd operation-prime and run "mvn clean install" command. this will also execute all the unit test for the project, elaborate unit test are written for the order api 

to cover as much cases as possible.

3. run "java -jar target/operation-prime-1.0-SNAPSHOT.jar" to run the application 

4. If everything works correctly, you can start the following URL.

  http://localhost:8080/operations

  
Using / testing the application

 1. Operation  screen you can easily add new operations with price and default wage and dlt charge  (Note always use numeric values for price, dlt and wage- no 

validation due to time constrain )
 
 2. Operation area screen - you can add multi operation area with special wage and dlt charge for each operation. 
                            if the area for an operation exists the  then wage and dlt will be updated else a new operation area mapping will be created 
							(Note always use numeric values for dlt and wage- no validation due to time constrain )

 3. Order screen - 		this screen can be used to create customer orders

  Application uses H2 database as this is in memory db any changes done on the application with be lost on restart
  
  I have taken the liberty to add some default data to test the below mentioned scenarios.
  
      a.  ----to test this ------ Discount 5% for the order which is amount paid over 2,000 baht
	             
				 1. go to order screen 
				 2. select operation = Servicing
				 3. select area  = pattaya 
				 
	 result --> price calulated will be 2090.00   
                ie. price + special wage + special dlt(as pattaya area has special price for this operation) = 2200 - 5% = 2090   	 
				
	   
	   b.  ----to test this ------ Assume that we have an operation called “Tax Extension”, we will do this operation free of charge if customer spend over 1,000 

baht 
	             
				 1. go to order screen 
				 2. select operation   = Tax Extension
				 3. select area = bangkok 
				 
	 result --> price calulated will be 0   (free)
                	 
				
	 c.  ----to test last case ------ no special area base wage  and no discount, just the price, wage and dlt from the operation  
	             
				 1. go to order screen 
				 2. select operation   = Servicing
				 3. select area = bangkok 
				 
	 result --> price calulated will be 1500   
                	 

